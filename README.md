# SvtPilot Backend

A Node.js App deployed to Heroku using [Express 4](http://expressjs.com/).

## Description

Backend Application that exposes a set of APIs toward the frontend App. The App is dedicated to editors at SVT and enables them to test their own TV programs. The tool allows editors to create surveys and send them out to a list of participants. It is an ideal tool to poll the audience's opinion and helps editors to improve their programs so that they fit the target audience's need. 

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Heroku Toolbelt](https://toolbelt.heroku.com/) installed.

```sh
$ git clone https://Root_22@bitbucket.org/Root_22/svt-svtpilot-api.git # or clone your own fork
$ cd svtpilotAPI/
$ npm install
$ npm start
```

The App is now running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```
$ git remote add heroku https://git.heroku.com/svtpilot-api.git
$ git push heroku master
$ heroku open
```

## Documentation

For more information about using Node.js on Heroku, see these Dev Center articles:

- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)
