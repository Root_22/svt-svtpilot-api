AwsController = function() {};

//var body_parser = require('body-parser')
//var http = require('http');
//var path = require('path');
var aws = require('aws-sdk');
var config = require('./config')();
var request = require('request');
var fs = require('fs');

AwsController.prototype.uploadFileAmazonS3 = function(file_path, file_name, file_type, file_size, callback) {

	aws.config.update({accessKeyId: config.AMAZON.AWS_KEY_ID, secretAccessKey: config.AMAZON.AWS_KEY_SECRET});
    var s3 = new aws.S3();
    var s3_params = {
        Bucket: config.AMAZON.BUCKET,
        Key: file_name,
        Expires: 60,
        ContentType: file_type,
        ACL: 'public-read'
    };
    s3.getSignedUrl('putObject', s3_params, function(err, data){
        if(err){
        	console.log("---> Error: Could not aquire signed url from Amazon S3.");
            console.log(err);
            callback({'success':false, 'url':null});
        }
        else{
        	var signed_request = data;
            var url = 'https://'+config.AMAZON.BUCKET+'.s3.amazonaws.com/'+file_name;
            console.log("---> Signed URL received: "+ signed_request);
            var upload_request = {method:'PUT', uri: signed_request, headers: {'x-amz-acl':'public-read','Content-Length':file_size}};//
            try{
            fs.createReadStream(file_path).pipe(request(upload_request, function(error, response, body){
            	if (!error && response.statusCode == 200) {
            		console.log('---> Uploaded successfully to Amazon S3..');
            		console.log('---> URL: '+url);
            		callback({'success':true, 'url':url});
            		//return {'success':true,'url':url};
            	}else{
            		console.log("---> Error: Could not upload file to Amazon S3.");
            		console.log('Status code: '+response.statusCode);
            		console.log(body);
            		if(!error){
            			console.log(error);
            			console.log('---------- Response Headers::');
            			console.log(response.headers);
            		}
            		callback({'success':false, 'url':null});
            	}
            }));
            }catch(err){
            	console.log(err);
            }

        }
        //return {'success':false,'url':null};
    });

}


AwsController.prototype.removeFileAmazonS3 = function(file_name, callback) {
	aws.config.update({accessKeyId: config.AMAZON.AWS_KEY_ID, secretAccessKey: config.AMAZON.AWS_KEY_SECRET});
    var s3 = new aws.S3();
    var params = { 
    	Bucket: config.AMAZON.BUCKET, 
    	Key: file_name 
    };
	s3.deleteObject(params, function(err, data) {
  		if(err){
  			console.log(err, err.stack);  // error
  			callback({'success':false});
  		}else{  
  			console.log('File removed from Amazon: '+file_name); // deleted
  			callback({'success':true});
  		}
	});

}



module.exports = new AwsController();