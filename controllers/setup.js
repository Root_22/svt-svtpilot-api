var db = require('pg');
var config = require('./config')();

InitController = function() {};

var getDate = function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; 
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 
    today = yyyy+'/'+mm+'/'+dd;
    return today;
}


InitController.prototype.setup = function() {
    db.connect(config.DATABASE.URL, function(err, client, done) {
        if(err){
            console.log(err);
        }
        client.query('CREATE TABLE IF NOT EXISTS account ( id bigserial primary key not null, username varchar(50) not null, password varchar(50) not null, created date not null, last_login date not null );'
                , function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE ACCOUNT CREATED');
                });// 

        client.query('CREATE TABLE IF NOT EXISTS program ( id bigint primary key not null, video_target_name varchar(50) not null, video_target_url varchar(200) not null, video_poster_url varchar(200), upload_date timestamp not null DEFAULT CURRENT_TIMESTAMP );'
                , function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE PROGRAM CREATED');
                });


        client.query('CREATE TABLE IF NOT EXISTS survey ( id bigserial primary key not null, name varchar(50) not null, publish_date date not null, program_id int not null, video_intro_url varchar(200) not null, public_access int not null DEFAULT 1, policy_lock int not null DEFAULT 0, author varchar(30), cint_full_url varchar(200), cint_out_url varchar(200), cint_complete_url varchar(200), FOREIGN KEY (program_id) REFERENCES program(id) ON DELETE CASCADE );'
                , function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE SURVEY CREATED');
                });

        client.query('CREATE TABLE IF NOT EXISTS question ( id bigserial primary key not null, type int not null, timestamp int, emoji_set int, content_text varchar(300), extra_text varchar(300), survey_id int not null, FOREIGN KEY (survey_id) REFERENCES survey(id) ON DELETE CASCADE );'
                , function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE QUESTION CREATED');
                });

        client.query('CREATE TABLE IF NOT EXISTS respondent ( id bigserial primary key not null, gender varchar(10) not null, age int not null, education varchar(100), family varchar(100), city varchar(100), survey_id int not null, FOREIGN KEY (survey_id) REFERENCES survey(id) ON DELETE CASCADE );'
                , function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE RESPONDENT CREATED');
                });

        client.query('CREATE TABLE IF NOT EXISTS answer ( id bigserial primary key not null, emoji_index int, content_text varchar(300), question_id int not null, respondent_id int not null, survey_id int not null, FOREIGN KEY (question_id) REFERENCES question(id) ON DELETE CASCADE, FOREIGN KEY (respondent_id) REFERENCES respondent(id) ON DELETE CASCADE, FOREIGN KEY (survey_id) REFERENCES survey(id) ON DELETE CASCADE);'
                , function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE ANSWER CREATED');
                });

        client.query('SELECT * FROM account', function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length==0){
                        client.query('INSERT INTO account (username, password, created, last_login) VALUES ($1,$2,$3,$4)',[config.ADMIN.ROOT.USERNAME, config.ADMIN.ROOT.PASSWORD, getDate(), getDate()],function(err, result) {
                            if (err)
                                console.log(err);
                        });
                        for(var i in config.ADMIN.ACCOUNTS){
                             client.query('INSERT INTO account (username, password, created, last_login) VALUES ($1,$2,$3,$4)',[config.ADMIN.ACCOUNTS[i].USERNAME, config.ADMIN.ACCOUNTS[i].PASSWORD, getDate(), getDate()],function(err, result) {
                                if (err)
                                    console.log(err);
                            });
                        }
                    }
        });
        // client.query('DELETE FROM survey WHERE id=$1', ['42'], function(err, result) {
        //             if (err)
        //                 console.log(err);
        //         });

    });

}

InitController.prototype.update = function() {
    db.connect(config.DATABASE.URL, function(err, client, done) {
        if(err){
            console.log(err);
        }
        client.query('INSERT INTO account (username, password, created, last_login) VALUES ($1,$2,$3,$4)',[config.ADMIN.TEST.USERNAME, config.ADMIN.TEST.PASSWORD, getDate(), getDate()],function(err, result) {
                            if (err)
                                console.log(err);
                        });
        // client.query('SELECT * FROM account', function(err, result) {
        //             if (err)
        //                 console.log(err);
        //             else if(result.rows.length>0){
        //                 client.query('UPDATE account SET username=$1, password=$2 WHERE id=$3',[config.ADMIN.ROOT.USERNAME, config.ADMIN.ROOT.PASSWORD, result.rows[0].id],function(err, result) {
        //                     if (err)
        //                         console.log(err);
        //                 });
        //             }
        //         });

        // client.query('ALTER TABLE respondent ADD cint_id varchar(200)',function(err, result) {
        //         if (err)
        //             console.log(err);
        // });
        // client.query('ALTER TABLE respondent DROP COLUMN postnumber',function(err, result) {
        //         if (err)
        //             console.log(err);
        // });

    });
};


InitController.prototype.cleanup = function() {
    db.connect(config.DATABASE.URL, function(err, client, done) {

        client.query('DROP TABLE IF EXISTS answer', function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE ANSWER DELETED');
                });

        client.query('DROP TABLE IF EXISTS respondent', function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE RESPONDENT DELETED');
                });

        client.query('DROP TABLE IF EXISTS question', function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE QUESTION DELETED');
                });

        client.query('DROP TABLE IF EXISTS survey', function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE SURVEY DELETED');
                });

        client.query('DROP TABLE IF EXISTS program', function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE PROGRAM DELETED');
                });

        client.query('DROP TABLE IF EXISTS account', function(err, result) {
                    if (err)
                        console.log(err);
                    else if(result.rows.length>0)
                        console.log('TABLE ACCOUNT DELETED');
                });

        done();



    });

}




module.exports = new InitController();