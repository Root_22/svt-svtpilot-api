PostController = function() {};
var qs = require('querystring');
var req = require('request');
var db = require('pg');
var config = require('./config')();
var fs = require('fs');
var header_data = {'Access-Control-Allow-Origin': '*','Access-Control-Allow-Methods': '*','Access-Control-Allow-Headers': '*' };

AwsController = require('./amazon');

var getDate = function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 
    today = yyyy+'/'+mm+'/'+dd;
    return today;
}


PostController.prototype.uploadFile = function(request, res) {
    try{
        var file = request.files.file;
        console.log(file.name);
        //console.log(file.type);
        console.log(file.path);
        var this_program_id = 1;

        db.connect(config.DATABASE.URL, function(err, client, done) {
            client.query('SELECT id FROM program ORDER BY id DESC LIMIT 1',function(err, result) {//
                var response = {};
                res.writeHead(200, header_data);
                if(err){
                    console.log(err);
                    response.success= false;
                    res.write(JSON.stringify(response, null, 2))
                    res.end();
                }else{
                    if (result.rows.length>0)
                        this_program_id = Number(result.rows[0].id) + 1;
                    console.log("this_program_id");
                    console.log(this_program_id);
                    var video_target_name = 'survey_video_'+this_program_id+'.'+file.path.split('.').pop();
                    var video_target_path = file.path.substring(0, file.path.lastIndexOf("/")+1)+video_target_name;
                    console.log(video_target_path);

                    fs.rename(file.path, video_target_path, function(err) {
                        if ( err ){
                            console.log('ERROR FS: ' + err);
                            response.success= false;
                            res.write(JSON.stringify(response, null, 2))
                            res.end();
                        }else{
                            client.query('INSERT INTO program (id, video_target_name, video_target_url, video_poster_url) VALUES ($1,$2,$3,$4) RETURNING id',[this_program_id, video_target_name, video_target_name, '../assets/images/video_start.png'],function(err, result) {   
                                if ( err ){
                                    console.log('ERROR DB 1: ' + err);
                                    response.success= false;
                                }else{
                                    response.message= "The file has been uploaded.";
                                    response.video_name= video_target_name;
                                    response.video_path= video_target_path;
                                    response.program_id= result.rows[0].id;
                                    response.success= true;
                                }     
                                res.write(JSON.stringify(response, null, 2))
                                res.end();
                        
                            });
                        }
                    });
                }
                done();
            });
        });
    }catch(err){
        console.log(err);
        res.writeHead(200, header_data);
        res.write(JSON.stringify({success:false}, null, 2))
        res.end();
    }
}


PostController.prototype.loginAdmin = function(request, res) {
    var playload = '';
    request.on('data', function (data) {
        playload += data;
    });
    request.on('end', function () {
        try{
            var params = JSON.parse(playload);
            db.connect(config.DATABASE.URL, function(err, client, done) {
                client.query('SELECT id, username, password FROM account', function(err, result) {
                    done();
                    var response = {};
                    response.success= false;
                    if(!err && result.rows.length>0 && params.username && params.password){ 
                        for(var i in result.rows){
                            if(params.username==result.rows[i].username && params.password==result.rows[i].password){
                                response.success= true;
                                break;
                            }
                        }
                    }
                    res.writeHead(200, header_data);
                    res.write(JSON.stringify(response, null, 2))
                    res.end();
                });
            });
        }catch(err){
            console.log(err);
            res.writeHead(200, header_data);
            res.write(JSON.stringify({success:false}, null, 2))
            res.end();
        }
    });
}


PostController.prototype.postSurvey = function(request, res) {
    var playload = '';
    request.on('data', function (data) {
        playload += data;
    });
    request.on('end', function () {
        try{
            var response = {};
            response.success= false;
            var params = JSON.parse(playload);
            var AwsCallback = function(aws_response){
                if(aws_response.success){
                    var video_url_aws = aws_response.url;
                    db.connect(config.DATABASE.URL, function(err, client, done){
                        client.query('INSERT INTO survey (name, publish_date, program_id, video_intro_url, policy_lock) VALUES ($1,$2,$3,$4,$5) RETURNING id',[params.name, getDate(), params.targetVideo.id, params.introVideoUrl, params.isLockPolicyApplied],function(err, result) {             
                            if(!err){
                                var survey_id= result.rows[0].id;
                                response.success= true;
                                response.id= survey_id;
                                client.query('UPDATE program SET video_target_url=$1 WHERE id=$2',[video_url_aws,params.targetVideo.id],function(err, result) {
                                }); 
                                var question_array = params.questions;
                                for(var i in question_array){
                                    var question = question_array[i];
                                    client.query('INSERT INTO question (type, timestamp, emoji_set, content_text, extra_text, survey_id) VALUES ($1,$2,$3,$4,$5,$6)',[question.type, question.timestamp, question.emojiset, question.text, question.extra, survey_id],function(err, result) {
                                    });
                                }        
                            }
                            done();
                            res.writeHead(200, header_data);
                            res.write(JSON.stringify(response, null, 2))
                            res.end();
            
                        });
                    });
                }else{
                    res.writeHead(200, header_data);
                    res.write(JSON.stringify(response, null, 2))
                    res.end();
                }
            }
            AwsController.uploadFileAmazonS3(params.targetVideo.path, params.targetVideo.name, params.targetVideo.type, params.targetVideo.size, AwsCallback);
        
        }catch(err){
            console.log(err);
            res.writeHead(200, header_data);
            res.write(JSON.stringify({success:false}, null, 2))
            res.end();
        }

    });
}


PostController.prototype.postCintLinks = function(request, res) {
    var playload = '';
    request.on('data', function (data) {
        playload += data;
    });
    request.on('end', function () {
        try{
            var params = JSON.parse(playload);
            db.connect(config.DATABASE.URL, function(err, client, done) {
                client.query('UPDATE survey SET cint_full_url=$1, cint_out_url=$2, cint_complete_url=$3 WHERE id=$4', [params.cintFullUrl, params.cintOutUrl, params.cintCompleteUrl, params.surveyId], function(err, result) {
                    done();
                    var response = {};
                    response.success= false;
                    if(!err){
                        response.success= true;
                    }
                    res.writeHead(200, header_data);
                    res.write(JSON.stringify(response, null, 2))
                    res.end();
                });
            });

        }catch(err){
            console.log(err);
            res.writeHead(200, header_data);
            res.write(JSON.stringify({success:false}, null, 2))
            res.end();
        }

    });
}


PostController.prototype.deleteSurvey = function(request, res) {
    var playload = '';
    request.on('data', function (data) {
        playload += data;
    });
    request.on('end', function () {
        try{
            var response = {};
            response.success= true;
            var params = JSON.parse(playload);
            db.connect(config.DATABASE.URL, function(err, client, done){
                client.query('SELECT program_id FROM survey WHERE id=$1',[params.id],function(err, result) {
                    if(err)
                        console.log(err);
                    else if(result.rows.length>0){
                        var programId = result.rows[0].program_id;
                        client.query('SELECT video_target_name,video_poster_url FROM program WHERE id=$1',[programId],function(err, result) {
                            if(err)
                                console.log(err);
                            else if(result.rows.length>0){
                                AwsController.removeFileAmazonS3(result.rows[0].video_target_name, function(aws_response){
                                    if(aws_response.success){
                                        client.query('DELETE FROM program WHERE id=$1',[programId],function(err, result) {
                                            if(err)
                                                console.log(err);
                                        });
                                    }
                                });
                                var posterFileName = result.rows[0].video_poster_url.split('/').pop();
                                AwsController.removeFileAmazonS3(posterFileName, function(aws_response){
                                });
                            }
                        });   
                    }
                });
                client.query('DELETE FROM survey WHERE id=$1',[params.id],function(err, result) {
                    if(err)
                        console.log(err);
                });
                done();
                res.writeHead(200, header_data);
                res.write(JSON.stringify(response, null, 2))
                res.end();
            });

        }catch(err){
            console.log(err);
            res.writeHead(200, header_data);
            res.write(JSON.stringify({success:false}, null, 2))
            res.end();
        }       
    });
}


PostController.prototype.deleteRespondent = function(request, res) {
    var playload = '';
    request.on('data', function (data) {
        playload += data;
    });
    request.on('end', function () {
        var response = {};
        response.success= true;
        try{
            var params = JSON.parse(playload);
            db.connect(config.DATABASE.URL, function(err, client, done){
                client.query('DELETE FROM respondent WHERE id=$1',[params.id],function(err, result) {
                    if(err){
                        console.log(err);
                        response.success= false;
                    }
                    done();
                    res.writeHead(200, header_data);
                    res.write(JSON.stringify(response, null, 2))
                    res.end();          
                });
                
            });

        }catch(err){
            console.log(err);
            res.writeHead(200, header_data);
            res.write(JSON.stringify({success:false}, null, 2))
            res.end();
        }       
    });
}



PostController.prototype.lockSurvey = function(request, res) {
    var playload = '';
    request.on('data', function (data) {
        playload += data;
    });
    request.on('end', function () {
        try{
            var response = {};
            response.success= true;
            var params = JSON.parse(playload);
            db.connect(config.DATABASE.URL, function(err, client, done){
                client.query('UPDATE survey SET public_access=0 WHERE id=$1',[params.id],function(err, result) {
                    done();
                    if(err){
                        console.log(err);
                        response.success= false;
                    }
                    res.writeHead(200, header_data);
                    res.write(JSON.stringify(response, null, 2))
                    res.end();
                });
            });

        }catch(err){
            console.log(err);
            res.writeHead(200, header_data);
            res.write(JSON.stringify({success:false}, null, 2))
            res.end();
        }    
    });
}


PostController.prototype.unlockSurvey = function(request, res) {
    var playload = '';
    request.on('data', function (data) {
        playload += data;
    });
    request.on('end', function () {
        try{
            var response = {};
            response.success= true;
            var params = JSON.parse(playload);
            var unlock = function(client){
                client.query('UPDATE survey SET public_access=1 WHERE id=$1',[params.id],function(err, result) {
                    var response = {};
                    response.success= true;
                    if(err){
                        console.log(err);
                        response.success= false;
                    }
                    res.writeHead(200, header_data);
                    res.write(JSON.stringify(response, null, 2))
                    res.end();
                });
            };
            db.connect(config.DATABASE.URL, function(err, client, done){
                client.query('SELECT policy_lock FROM survey WHERE id=$1', [params.id],function(err, result) {
                    if(err){
                        console.log(err);
                        done();
                        res.writeHead(200, header_data);
                        res.write(JSON.stringify({'success': false}, null, 2))
                        res.end();
                    }else{
                        if(result.rows[0].policy_lock){
                            console.log('lock policy applied');
                            client.query('SELECT * FROM respondent WHERE survey_id=$1', [params.id],function(err, result) {
                                if(err){
                                    console.log(err);
                                    res.writeHead(200, header_data);
                                    res.write(JSON.stringify({'success': false}, null, 2))
                                    res.end();
                                }else if(result.rows.length>=50){
                                    console.log('limit reached');
                                    res.writeHead(200, header_data);
                                    res.write(JSON.stringify({'success': false, 'errorMsg': 'Undersökningen kan inte bli publik, visning gränsen har nåtts.'}, null, 2))
                                    res.end();
                                }else
                                    unlock(client);
                                done();
                            }); 
                        }else{
                            unlock(client);
                            done();
                        }
                    }
            
                });  
            });
        }catch(err){
            console.log(err);
            res.writeHead(200, header_data);
            res.write(JSON.stringify({success:false}, null, 2))
            res.end();
        } 
    });
}


PostController.prototype.postRespondent = function(request, res) {
    var playload = '';
    request.on('data', function (data) {
        playload += data;
    });
    request.on('end', function () {
        try{
            var params = JSON.parse(playload);
            db.connect(config.DATABASE.URL, function(err, client, done){
                var respondent = params.profile;
                client.query('INSERT INTO respondent (gender, age, education, family, city, cint_id, survey_id) VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING id',[respondent.gender, Number(respondent.age), respondent.education, respondent.family, respondent.residence, respondent.cint_id, params.survey_id],function(err, result) {
                    var response = {};
                    response.success= false;
                    if(!err){
                        var respondent_id= result.rows[0].id;

                        var answer_array = params.answers;
                        for(var i in answer_array){
                            var answer = answer_array[i];
                            client.query('INSERT INTO answer (emoji_index, content_text, question_id, respondent_id, survey_id) VALUES ($1,$2,$3,$4,$5)',[answer.emoji_index, answer.content_text, answer.question_id, respondent_id, params.survey_id],function(err, result) {
                                if(err)
                                    console.log(err);
                            });
                        }  
                        response.success= true;
                        response.respondent_id = respondent_id;
                        res.writeHead(200, header_data);
                        res.write(JSON.stringify(response, null, 2))
                        res.end();

                        client.query('SELECT policy_lock FROM survey WHERE id=$1', [params.survey_id],function(err, result) {
                            if(err){
                                console.log(err);
                                done();
                            }else if(result.rows[0].policy_lock){
                                client.query('SELECT * FROM respondent WHERE survey_id=$1', [params.survey_id],function(err, result) {
                                    if(err)
                                        console.log(err);
                                    else if(result.rows.length>50){
                                        client.query('UPDATE survey SET public_access=$1 WHERE id=$2',[0,params.survey_id],function(err, result) {
                                            if(err)
                                                console.log(err)
                                        });
                                    }
                                    done();
                                });  
                            }else
                                done();
                        });    
                    }else{
                        done();
                        res.writeHead(200, header_data);
                        res.write(JSON.stringify(response, null, 2))
                        res.end();
                    }
                });
            });
        
        }catch(err){
            console.log(err);
            res.writeHead(200, header_data);
            res.write(JSON.stringify({success:false}, null, 2))
            res.end();
        } 
        
    });
}


PostController.prototype.postAnswers = function(request, res) {
    var playload = '';
    request.on('data', function (data) {
        playload += data;
    });
    request.on('end', function () {
        try{
            var response = {};
            response.success= true;
            var params = JSON.parse(playload);
            if(!params.respondent_id){
                response.success= false;
                res.writeHead(200, header_data);
                res.write(JSON.stringify(response, null, 2))
                res.end();
                return;
            }
            db.connect(config.DATABASE.URL, function(err, client, done){
                var answer_array = params.answers;
                for(var i in answer_array){
                    var answer = answer_array[i];
                    client.query('INSERT INTO answer (emoji_index, content_text, question_id, respondent_id, survey_id) VALUES ($1,$2,$3,$4,$5)',[answer.emoji_index, answer.content_text, answer.question_id, params.respondent_id, params.survey_id],function(err, result) {
                        if(err)
                            console.log(err);
                    });
                }  
                done();
                response.respondent_id = params.respondent_id;
                res.writeHead(200, header_data);
                res.write(JSON.stringify(response, null, 2))
                res.end();
            });

        }catch(err){
            console.log(err);
            res.writeHead(200, header_data);
            res.write(JSON.stringify({success:false}, null, 2))
            res.end();
        } 
            
    });
}


PostController.prototype.postThumbnail = function(request, res) {
    try{
        var file = request.files.file;
        var filesize = file.size;
        console.log(file.name);
        //console.log(file.type);
        //console.log(file.path);
        console.log(request.body.surveyId);
        var newFilePath = './assets/poster/'+request.body.fileName;
        fs.rename('./'+file.path, newFilePath, function(err) {
            var response = {};
            if ( err ){
                console.log('ERROR: ' + err);
                response.success= false;
                res.writeHead(200, header_data);
                res.write(JSON.stringify(response, null, 2))
                res.end();
            }else{
                var AwsCallback = function(aws_response){
                    if(aws_response.success){
                        var poster_url_aws = aws_response.url;
                        db.connect(config.DATABASE.URL, function(err, client, done){
                            client.query('SELECT program_id FROM survey WHERE id=$1',[request.body.surveyId],function(err, result) {
                                if(!err && result.rows.length>0){
                                    client.query('UPDATE program SET video_poster_url=$1 WHERE id=$2',[poster_url_aws,result.rows[0].program_id],function(err, result) {
                                        if ( err ){
                                            console.log('ERROR: ' + err);
                                            response.success= false;
                                        }else{
                                            response.success= true;
                                        }
                                        done();
                                        res.writeHead(200, header_data);
                                        res.write(JSON.stringify(response, null, 2))
                                        res.end();
                                    });
                            
                                }else {
                                    if(err)
                                        console.log(err);
                                    done();
                                    response.success= false;
                                    res.writeHead(200, header_data);
                                    res.write(JSON.stringify(response, null, 2))
                                    res.end();
                                }
                            });

                        });
                    }else{
                        response.success= false;
                        res.writeHead(200, header_data);
                        res.write(JSON.stringify(response, null, 2))
                        res.end();
                    }
                } 

                AwsController.uploadFileAmazonS3(newFilePath, request.body.fileName, 'image/png', filesize, AwsCallback);
            }
                
        });

    }catch(err){
        console.log(err);
        res.writeHead(200, header_data);
        res.write(JSON.stringify({success:false}, null, 2))
        res.end();
    } 

}


module.exports = new PostController();