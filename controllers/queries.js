GetController = function() {};

var request = require('request');
var db = require('pg');
var config = require('./config')();
var header_data = {'Access-Control-Allow-Origin': '*','Access-Control-Allow-Methods': '*','Access-Control-Allow-Headers': '*' };


GetController.prototype.listAccounts = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
	client.query('SELECT username FROM account', function(err, result) {
		done();
  		var response = {};
  		if (err){
  			response.success =  false;
  		}else{
			response.success =  true;
			response.content = result.rows;
		}

		res.writeHead(200, header_data);
		res.write(JSON.stringify(response, null, 2));
    	res.end();
	});
	});
}



GetController.prototype.listSurveys = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
	client.query('SELECT survey.id,survey.name,survey.publish_date,program.video_poster_url FROM survey, program WHERE survey.program_id=program.id ORDER BY publish_date DESC', function(err, result) {
		done();
  		var response = {};
  		if (err){
  			response.success =  false;
  		}else{
			response.success =  true;
			response.content = result.rows;
		}

		res.writeHead(200, header_data);
		res.write(JSON.stringify(response, null, 2));
    	res.end();
	});
	});
}

GetController.prototype.querySurveyUrls = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
	client.query('SELECT survey.video_intro_url, program.video_target_url, survey.public_access, survey.cint_full_url, survey.cint_out_url, survey.cint_complete_url FROM survey, program WHERE survey.id=$1 AND survey.program_id=program.id', [req.params.id],function(err, result) {
  		done();
  		var response = {};
  		if (err){
  			response.success =  false;
  		}else{
			response.success =  true;
			response.content = result.rows;
		}
		res.writeHead(200, header_data);
		res.write(JSON.stringify(response, null, 2));
    	res.end();
	});

	});
}


GetController.prototype.querySurveyQuestions = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
		client.query('SELECT id,type,timestamp,emoji_set,content_text,extra_text FROM question WHERE survey_id=$1', [req.params.id],function(err, result) {
	  		done();
	  		var response = {};
	  		if (err){
	  			response.success =  false;
	  		}else{
				response.success =  true;
				response.content = result.rows;
			}
			res.writeHead(200, header_data);
			res.write(JSON.stringify(response, null, 2));
	    	res.end();
		});
	});
	
}


GetController.prototype.querySurveyAnswers = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
		client.query('SELECT id,emoji_index,content_text,question_id,respondent_id FROM answer WHERE survey_id=$1', [req.params.id],function(err, result) {
	  		done();
	  		var response = {};
	  		if (err){
	  			response.success =  false;
	  		}else{
				response.success =  true;
				response.content = result.rows;
			}
			res.writeHead(200, header_data);
			res.write(JSON.stringify(response, null, 2));
	    	res.end();
		});
	});
	
}



GetController.prototype.queryAllRespondents = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
		client.query('SELECT id,age,gender,family,education,city,cint_id FROM respondent WHERE survey_id=$1', [req.params.id],function(err, result) {
			done();
	  		var response = {};
	  		if (err){
	  			response.success =  false;
	  		}else{
				response.success =  true;
				response.content = result.rows;
			}
			res.writeHead(200, header_data);
			res.write(JSON.stringify(response, null, 2));
	    	res.end();
		});
	});
	
}


GetController.prototype.queryParticipantsStatistics = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
	client.query('SELECT age,gender,family,education,city FROM respondent WHERE survey_id=$1', [req.params.id],function(err, result) {
		done();
  		var response = {};
  		if (err){
  			response.success =  false;
  		}else{
			response.success =  true;
			response.content = {};
			response.content.total = result.rows.length;
			response.content.gender = {};
			response.content.gender.male = result.rows.filter(function(x){if(x.gender=="male")return true;}).length;
			response.content.gender.female = result.rows.filter(function(x){if(x.gender=="female")return true;}).length;
			response.content.gender.other = result.rows.filter(function(x){if(x.gender=="other")return true;}).length;
			response.content.age = [];
			
			var ageDistribution = [{min: 10, max: 19},{min: 20, max: 29},{min: 30, max: 39},{min: 40, max: 49},{min: 50, max: 59},{min: 60, max: 69},{min: 70, max: 79},{min: 80, max: 99}];
			for(var i in ageDistribution){
				var category_entry = {}
				category_entry.category = ageDistribution[i].min+"-"+ageDistribution[i].max+" years";
				category_entry.total = result.rows.filter(function(x){if(x.age>=ageDistribution[i].min && x.age<=ageDistribution[i].max)return true;}).length;
				category_entry.male = result.rows.filter(function(x){if(x.age>=ageDistribution[i].min && x.age<=ageDistribution[i].max && x.gender=="male")return true;}).length;
				category_entry.female = result.rows.filter(function(x){if(x.age>=ageDistribution[i].min && x.age<=ageDistribution[i].max && x.gender=="female")return true;}).length;
				category_entry.other = result.rows.filter(function(x){if(x.age>=ageDistribution[i].min && x.age<=ageDistribution[i].max && x.gender=="other")return true;}).length;
				response.content.age.push(category_entry);
			}

			response.content.family = {};
			response.content.family.not_parent = result.rows.filter(function(x){if(x.family=="Inga barn < 19")return true;}).length;
			response.content.family.junior_parent = result.rows.filter(function(x){if(x.family=="Med barn < 6")return true;}).length;
			response.content.family.senior_parent = result.rows.filter(function(x){if(x.family=="Med barn 7-18")return true;}).length;

			response.content.residence = {};
			response.content.residence.big_city = result.rows.filter(function(x){if(x.city=="Storstad")return true;}).length;
			response.content.residence.normal_city = result.rows.filter(function(x){if(x.city=="Stad")return true;}).length;
			response.content.residence.small_city = result.rows.filter(function(x){if(x.city=="Mindre stad")return true;}).length;
			response.content.residence.tiny_city = result.rows.filter(function(x){if(x.city=="Mindre samhälle")return true;}).length;

			response.content.education = {};
			response.content.education.high_degree = result.rows.filter(function(x){if(x.education=="Högskola/Universitet")return true;}).length;
			response.content.education.low_degree = result.rows.filter(function(x){if(x.education=="Grundskola/Gymnasiet")return true;}).length;
			response.content.education.no_degree = result.rows.filter(function(x){if(x.education=="Studerar fortfarande")return true;}).length;
			response.content.education.undefined_degree = result.rows.filter(function(x){if(x.education=="Vill ej uppge")return true;}).length;
		}
		res.writeHead(200, header_data);
		res.write(JSON.stringify(response, null, 2));
    	res.end();
	});
	});
	
}

var applyQueryFilter = function(element, queryFilter){
	if(!queryFilter){
		return true;
	}
	if(queryFilter.age_min && element.age<Number(queryFilter.age_min)){
		return false;
	}
	if(queryFilter.age_max && element.age>Number(queryFilter.age_max)){
		return false;
	}
	if(queryFilter.education && element.education!=queryFilter.education){
		return false;
	}
	if(queryFilter.family && element.family!=queryFilter.family){
		return false;
	}
	if(queryFilter.city && element.city!=queryFilter.city){
		return false;
	}
	return true;
};

var buildQuestionStatsResponse = function(rows, questionId, requestFilter){
	var response = {};
	response.success =  true;
	response.questionId = Number(questionId);
	response.statistics = {};
	response.statistics.total = [];
	response.statistics.male = [];
	response.statistics.female = [];
	response.statistics.other= [];
	var queryFilter;
	if (Object.keys(requestFilter).length > 0){
		queryFilter = requestFilter;
	}

	var allAnswersCount = rows.filter(function(x){if(applyQueryFilter(x,queryFilter))return true;}).length;
	var maleAnswersCount = rows.filter(function(x){if(x.gender=="male" && applyQueryFilter(x,queryFilter))return true;}).length;
	var femaleAnswersCount = rows.filter(function(x){if(x.gender=="female" && applyQueryFilter(x,queryFilter))return true;}).length;
	var otherAnswersCount = rows.filter(function(x){if(x.gender=="other" && applyQueryFilter(x,queryFilter))return true;}).length;

	for(var i=1;i<=5;i++){
		if(allAnswersCount>0)
			response.statistics.total.push(Number((rows.filter(function(x){if(x.emoji_index==i && applyQueryFilter(x,queryFilter))return true;}).length/allAnswersCount).toFixed(2)));
		else
			response.statistics.total.push(0);

		if(maleAnswersCount>0)
			response.statistics.male.push(Number((rows.filter(function(x){if(x.emoji_index==i && x.gender=="male" && applyQueryFilter(x,queryFilter))return true;}).length/maleAnswersCount).toFixed(2)));
		else
			response.statistics.male.push(0);

		if(femaleAnswersCount>0)
			response.statistics.female.push(Number((rows.filter(function(x){if(x.emoji_index==i && x.gender=="female" && applyQueryFilter(x,queryFilter))return true;}).length/femaleAnswersCount).toFixed(2)));
		else
			response.statistics.female.push(0);

		if(otherAnswersCount>0)
			response.statistics.other.push(Number((rows.filter(function(x){if(x.emoji_index==i && x.gender=="other" && applyQueryFilter(x,queryFilter))return true;}).length/otherAnswersCount).toFixed(2)));
		else
			response.statistics.other.push(0);

	}
	return response;
}


GetController.prototype.queryEmojiQuestionStatistics = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
		client.query('SELECT answer.emoji_index, respondent.gender, respondent.age, respondent.education, respondent.family, respondent.city FROM answer, respondent WHERE answer.question_id=$1 AND answer.respondent_id=respondent.id', [req.params.id],function(err, result) {
	  		done();
	  		var response = {};
	  		if (err){
	  			response.success =  false;
	  		}else{
				response = buildQuestionStatsResponse(result.rows, req.params.id, req.query);
			}
			res.writeHead(200, header_data);
			res.write(JSON.stringify(response, null, 2));
	    	res.end();
		});
	});
}



GetController.prototype.queryQuestionAnswers = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
		var queryExtra = "";
		if(req.query && req.query.limit && req.query.offset)
			queryExtra = ' LIMIT '+req.query.limit+' OFFSET '+req.query.offset;
		client.query('SELECT answer.content_text, answer.emoji_index, respondent.gender, respondent.age, respondent.education, respondent.family, respondent.city, respondent.id FROM answer, respondent WHERE answer.question_id=$1 AND answer.respondent_id=respondent.id'+queryExtra, [req.params.id],function(err, result) {
	  		done();
	  		var response = {};
	  		if (err){
	  			response.success =  false;
	  		}else{
				response.success =  true;
				response.answers = result.rows.filter(function(x){return applyQueryFilter(x,req.query);});	
			}
			res.writeHead(200, header_data);
			res.write(JSON.stringify(response, null, 2));
	    	res.end();
		});
	});
}


GetController.prototype.orderRespondents = function(req, res) {
	db.connect(config.DATABASE.URL, function(err, client, done) {
		client.query('SELECT id,type,timestamp,emoji_set,content_text,extra_text FROM question WHERE survey_id=$1', [req.params.id],function(err, result) {
	  		var response = {};
	  		if (err){
	  			done();
	  			response.success =  false;
	  			res.write(JSON.stringify(response, null, 2));
    			res.end();
	  		}else{
				response.success =  true;
				response.content = result.rows;
				var questionListAll = result.rows;
				var magicMatch = questionListAll.filter(function(x){if(x.id===req.params.magicQuestionId)return true; });
				if(magicMatch.length>0){
        			var magicQuestion = magicMatch[0];
        			client.query('SELECT id,emoji_index,content_text,question_id,respondent_id FROM answer WHERE survey_id=$1', [req.params.id],function(err, result) {
				  		var response = {};
				  		if (err){
				  			done();
				  			response.success =  false;
				  			res.writeHead(200, header_data);
							res.write(JSON.stringify(response, null, 2));
				    		res.end();
				  		}else{
							response.success =  true;
							var allAnswers = result.rows;
							var magicAnswers = allAnswers.filter(function(x){if(x.question_id==magicQuestion.id)return true;});
                    		magicAnswers.sort(function(a1, a2){return a1.emoji_index-a2.emoji_index});
                            client.query('SELECT id,age,gender,family,education,city,cint_id FROM respondent WHERE survey_id=$1', [req.params.id],function(err, result) {
								done();
						  		var response = {};
						  		if (err){
						  			response.success =  false;
						  			res.writeHead(200, header_data);
									res.write(JSON.stringify(response, null, 2));
						    		res.end();
						  		}else{
									response.success =  true;
									var participants = result.rows;
									var sortedParticipants = [];
									if(participants.length>0){
				                        for(var i=0;i<magicAnswers.length;i++){
				                            var pMatches = participants.filter(function(x){if(x.id==magicAnswers[i].respondent_id)return true;});
				                            if(pMatches && pMatches.length>0){
				                                var personOfInterest = pMatches[0];
				                                sortedParticipants.push(personOfInterest);
				                            }
				                        }
				                    }
				                    response.content = sortedParticipants;

				                    res.writeHead(200, header_data);
									res.write(JSON.stringify(response, null, 2));
						    		res.end();


								}
								
							});

						}
						
					});

        		}else{
        			done();
        			response.success =  true;
	  				res.write(JSON.stringify(response, null, 2));
    				res.end();
        		}

				// var excel = exportAll();
				// res.writeHead(200, {'Content-Type': 'text/html','Content-Length': excel.length});
	   //  		res.end(excel);
			}
			
		});

	});
}







module.exports = new GetController();