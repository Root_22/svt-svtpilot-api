var config = null;
module.exports = function () {
    config = {
    	DATABASE: {
            	URL:   process.env.DATABASE_URL +'?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory'
    		},
        ADMIN: {
            ROOT:{
                USERNAME:   'svtpilot_root',
                PASSWORD:   '+d5!kJZKwTB}kgGX'
            },
            TEST:{
                USERNAME:   'svtpilot_test',
                PASSWORD:   'svtpilot_test22!'
            },
            ACCOUNTS:[{
                USERNAME:   'Dokumentär Stockholm',
                PASSWORD:   'ynSCUY34y(!tvD<('
            },{
                USERNAME:   'Kultur Stockholm',
                PASSWORD:   'np+6"h=q3)3&uVn3'
            },{
                USERNAME:   'Nöje Stockholm',
                PASSWORD:   'v=vF*L=!5vdqRRzP'  
            },{
                USERNAME:   'Digitalt Stockholm',
                PASSWORD:   '5PhwH=S]+LCAM!3M'
            },{
                USERNAME:   'Nyheterna Stockholm',
                PASSWORD:   'S.!R?+Nm~7va?<CL'
            },{
                USERNAME:   'Sporten Stockholm',
                PASSWORD:   ',[+CnNW2qr,Myq#<'
            },{
                USERNAME:   'Drama Stockholm',
                PASSWORD:   'vFb94e#@b$(dZL%3'
            },{
                USERNAME:   'Utvecklingen Malmö',
                PASSWORD:   '!aAUZnr+Qn@QG5_:'
            },{
                USERNAME:   'Digitalt Malmö',
                PASSWORD:   '2rBvk^>zv];TAK]D'
            },{
                USERNAME:   'Utvecklingen Göteborg',
                PASSWORD:   '{FcvT!y+U*>qG4b&'
            },{
                USERNAME:   'Samhälle Göteborg',
                PASSWORD:   'W92&cC5,/XewZ#A>'
            },{
                USERNAME:   'Utvecklingen Umeå',
                PASSWORD:   'B2k_D[}bXge.+8d#'
            }]
        },
        AMAZON: {
            BUCKET: process.env.S3_BUCKET_NAME,
            AWS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
            AWS_KEY_SECRET: process.env.AWS_SECRET_ACCESS_KEY
        },
        AUTH: {
            username:   'svtpilot',
            password:   ']!u_GVqU7;a,d9Qe'
        }
	};
    return config;
};