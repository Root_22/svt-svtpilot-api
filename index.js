var config = require('./controllers/config')();
var auth = require('basic-auth');
var express = require('express');
var querystring = require('querystring');
var app = express();

app.set('port', (process.env.PORT || 5000));

app.options('*', function(req, res){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "*");
	res.header("Access-Control-Allow-Headers", "Content-Type,Authorization");
	res.header("Accept", "POST");
	res.header("Content-Type", "*");
	res.status(200);
	res.write('POST');
	res.end();
});

app.use(function(req, res, next) {
    if(String(req.originalUrl).substring(0, 7) !== '/assets'){
        var user = auth(req);
        //console.log(user);
        if (!user || config.AUTH.username !== user.name || config.AUTH.password !== user.pass) {
            res.set('WWW-Authenticate', 'Basic realm="kanslokoll-api"');
            return res.status(401).send();
        }else{
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "X-Requested-With");
            next();
        }
    }else{
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        next();
    }  
});

app.use("/assets",express.static(__dirname + '/assets')); //assets/media/survey_video.mp4

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

// Requires multiparty 
multiparty = require('connect-multiparty');
multipartyMiddleware = multiparty();

// Requires controller
InitController = require('./controllers/setup');
GetController = require('./controllers/queries');
PostController = require('./controllers/submit');

//InitController.cleanup();
//InitController.setup();
//InitController.update();

/**  GET  **/

app.get('/surveytool-api/query/getAllAccounts', GetController.listAccounts);

app.get('/surveytool-api/query/getAllSurveys', GetController.listSurveys);

app.get('/surveytool-api/query/getSurveyUrls/:id', GetController.querySurveyUrls);

app.get('/surveytool-api/query/getSurveyQuestions/:id', GetController.querySurveyQuestions);

app.get('/surveytool-api/query/getAllRespondents/:id', GetController.queryAllRespondents);

app.get('/surveytool-api/query/getSurveyAnswers/:id', GetController.querySurveyAnswers);

app.get('/surveytool-api/query/getParticipantStats/:id', GetController.queryParticipantsStatistics);

app.get('/surveytool-api/query/getEmojiQuestionStats/:id' , GetController.queryEmojiQuestionStatistics);

app.get('/surveytool-api/query/getQuestionAnswers/:id' , GetController.queryQuestionAnswers);

app.get('/surveytool-api/query/getSurveyRespondents/:id/orderBy/:magicQuestionId', GetController.orderRespondents);

/**  POST  **/

app.post('/surveytool-api/post/postVideo', multiparty({ uploadDir: './assets/media/' }), PostController.uploadFile);

app.post('/surveytool-api/post/loginAdmin', PostController.loginAdmin);

app.post('/surveytool-api/post/postSurvey', PostController.postSurvey);

app.post('/surveytool-api/post/postCintLinks', PostController.postCintLinks);

app.post('/surveytool-api/post/deleteSurvey', PostController.deleteSurvey);

app.post('/surveytool-api/post/deleteRespondent', PostController.deleteRespondent);

app.post('/surveytool-api/post/lockSurvey', PostController.lockSurvey);

app.post('/surveytool-api/post/unlockSurvey', PostController.unlockSurvey);

app.post('/surveytool-api/post/postRespondent', PostController.postRespondent);

//app.post('/surveytool-api/post/postAnswers', PostController.postAnswers);

app.post('/surveytool-api/post/postSurveyPoster', multiparty({ uploadDir: './assets/poster/' }), PostController.postThumbnail);


app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


